//npm init -y - skips all the prompt and goes directly into installing npm/ creating package.json

/*
	create an express variable that accepts a require("express") value
	store the "express()" function inside the "app"variable
	create a port variable that accepts 3000
	make your app able to read json as well as accept data from forms

	make your app listen/run to the port variable with a confirmation in the console "server is running at port"
*/
const express = require("express");

//mongoose - a package that allows creation of schemas to model the data structure and to have an access to a number of methods for manipulating
const mongoose = require("mongoose");

const app = express();

const port = 3000;

app.use(express.json());

app.use(express.urlencoded({extended:true}));

//<username> - change into the correct username in your MongoDB account that has ADMIN function
//<password> - change into the password of the ADMIN
//"myFirstDatabase" - the name of the database that will be created. (changed into "b170-to-do")
mongoose.connect("mongodb+srv://eltonUser:chicharon@cluster0.7jgiw.mongodb.net/b170-to-do?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
})
// notification for connection: success/faulure
let db=mongoose.connection;
// if an error existed in connecting to MongoDB
db.on("error", console.error.bind(console, "Connection Error"));
// if the connection is successful
db.once("open",() => console.log("We're connected to the database"))

// Mongoose Schema - sets the structure of the document that is to be created; serves as the blueprint to the data/record


const taskSchema=new mongoose.Schema({
	name: String,
	status:{
		type: String,
		// default - sets the value once the field does not have any value entered in it
		default: "pending"
	}
})

const registrationSchema= new mongoose.Schema({
	userName: String,
	password: String
})

/*model - allows access to methods that will perform CRUD operations in the database;
RULE: the first letter is always capital/ uppercase for the variable of the model and must be singular form

SYNTAX
const<Variable>= mongoose.model("<Variable>", <schemaName>)
*/

const Task= mongoose.model("Task", taskSchema)
const Registration= mongoose.model("Registration", registrationSchema)
//routes
/*
business logic:
1. check if the task is already existing
	if the task exists, return "there is a duplicate task"
	if it is not existing, add the task in the database

2. the task will come from the request body

3. create a new task object with the needed properties

4. save to db
*/

app.post("/tasks",(req,res)=>{
	//checking for duplicate tasks
	//findOne is a mongoose method that acts similar to find in MongoDB; returns the first document it finds

	Task.findOne({name:req.body.name}, (error,result)=>{
		// if the "result" has found a task, it will return the res.send


		if(result !== null &&result.name===req.body.name){
			return res.send("There is a duplicate task");
		}else{
			let newTask = new Task ({
				name:req.body.name
			})
			//saveErr - parameter that accepts errors, should there be any when saving the "newTask" object
			//saveTask - parameter that accepts the object should the saving of the "newTask" object is a success
			newTask.save((saveErr,savedTask)=>{
				if(saveErr){
					return console.error(saveErr);
				}else{
					//.status - returns a status (number code such as 201 for successful creation)
					return res.status(201).send("New Task Created")
				}
			})
		}
	})
})

//business logic
/*
1. retrieve all the documents using the find() functionality
2. if an error is encountered, print the error (error handling)
3. if there are no errors, send a success status back to the client and return an array of documents*/

app.get("/tasks",(req,res)=>{
	// find is similar to the mongodb/robo3t find. setting up emplty field in "{}" would allow the app to find all documents inside the database
	Task.find({},(error,result)=>{
		if (error){
			return console.log(error)
		}else{
			return res.status(200).json({data:result}) // data field will be set and its value will be the result/ response that we had for the request
		}


	})
})

app.post("/registration",(req,res)=>{
	Task.findOne({userName:req.body.userName}, (errors,results)=>{
		if (results !== null && results.userName==req.body.userName){
		return res.send("username already in use");
		}else{
			let newRegistration = new Registration ({
				userName: req.body.userName,
				password: req.body.password
			})
			newRegistration.save((saveError,savedRegistration)=>{
				if (saveError){
					return console.error(saveError);
				}else{
					return res.status(201).send("successfully registered")
				}
			})
		}
	})
})

app.get("/registration",(req,res)=>{
	Task.find({},(errors,results)=>{
		if (errors){
			return console.log(errors)
		}else{
			return res.status(200).json({data:results}) 
		}


	})
})











app.listen(port,() => console.log(`Server is running at port ${port}`));